// ==== Problem #1 ====
// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of:
//

function prob1(inventory) {
  let x = inventory.find((x) => {
    return x.id === 33;
  });
  console.log(
    `Car 33  was made in ${x.car_year}  and it is a ${x.car_make} of  ${x.car_model}  model`
  );
}
module.exports = prob1;
