# Drill4-Js

This project contains a set of JavaScript exercises aimed at practicing various programming concepts.

### Installation

1. Clone this repository: `git clone https://gitlab.com/jaspreet-singh2/Drill5-js.git`
2. Navigate to the project directory: `cd Drill5-js`
3. Install dependencies: `npm install`
4. Run the tests:`node problemtest.js`

Follow the prompts to interact with the program and solve the JavaScript problems.

## Problems

Here are the problems included in this project:

### Problem #1

The dealer needs to find information about a car with an id of 33. Implement a function to retrieve and display the car's year, make, and model.

### Problem #2

The dealer wants to know the make and model of the last car in the inventory. Implement a function to display this information.

### Problem #3

The marketing team requires a sorted list of car models for the website. Implement a function to sort the car model names alphabetically.

### Problem #4

The accounting team needs a list of all car years. Implement a function to extract and display an array containing only the car years.

### Problem #5

The car lot manager needs to determine the number of cars older than the year 2000. Implement a function to find and display the count of cars made before 2000.

### Problem #6

A buyer is interested in seeing only BMW and Audi cars within the inventory. Implement a function to filter and display an array containing only BMW and Audi cars.


## License

This project is licensed under the [MIT License](LICENSE).
