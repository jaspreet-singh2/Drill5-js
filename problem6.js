// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.

function prob6(inventory) {
  let audiBmw = [];
  audiBmw = inventory.filter((x) => {
    if (x.car_make == "Audi" || x.car_make == "BMW") {
      return x;
    }
  });
   return audiBmw
  
}
module.exports = prob6;
