// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

function alphaSort(a, b) {
  return a.car_model.localeCompare(b.car_model);
}

function prob3(array) {
  array.sort(alphaSort);
  array.forEach((element) => {
    console.log(element.car_model);
  });
}

module.exports = prob3;
